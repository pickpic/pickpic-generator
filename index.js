const max = 1000
const subs = ['puppy', 'cats']
const path = require('path')
const imgdir = path.resolve('./images')
const async = require('async')
const downloader = require('image-downloader')
const puppy = require('random-puppy')
const pace = require('pace')(max)
const fs = require('fs-extra')
const _ = require('lodash')
if (!fs.existsSync(imgdir)) fs.mkdirpSync(imgdir)
const makeid = () => {
  let text = ""
  const possible = 'abcdefghijklmnopqrstuvwxyz'
  for (var i = 0; i < 5; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length))
  }
  return text
}
const arr = _.range(max)
const chunks = _.chunk(arr, 100)
async.eachSeries(chunks, (chunk, nextchunk) => {
  async.eachLimit(chunk, 10, (p, nextp) => {
    puppy(subs[_.random(subs.length)])
      .then(url => {
        return downloader.image({
          url,
          dest: `${imgdir}/${makeid()}.png`
        })
      }).then(() => {
        pace.op()
        nextp()
      })
  }, () => {
    setTimeout(() => {
      nextchunk()
    }, 2000)    
  })
}, () => {
  console.log('Process complete')
})
